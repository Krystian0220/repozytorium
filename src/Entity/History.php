<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HistoryRepository")
 * @ORM\Table(name="history")
 */
class History
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="first_in")
     */
    private $firstIn;

    /**
     * @ORM\Column(type="integer", name="second_in")
     */
    private $secondIn;

    /**
     * @ORM\Column(type="integer", name="first_out")
     */
    private $firstOut;

    /**
     * @ORM\Column(type="integer", name="second_out")
     */
    private $secondOut;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     */
    private $updatedAt;

    // Getter i setter dla każdego pola...

    // Przykładowe metody getter i setter dla pola 'firstIn':
    public function getFirstIn(): ?int
    {
        return $this->firstIn;
    }

    public function setFirstIn(int $firstIn): self
    {
        $this->firstIn = $firstIn;

        return $this;
    }
}
