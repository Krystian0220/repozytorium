<?php // src/Controller/ExchangeController.php
namespace App\Controller;

use App\Entity\History;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeController extends AbstractController
{
    private $entityManager;
    private $paginator;

    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/exchange/values", name="exchange_values", methods={"POST"})
     */
    public function exchangeValues(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['first']) || empty($data['second'])) {
            return $this->json(['error' => 'Missing required fields'], 400);
        }

        $firstValue = (int) $data['first'];
        $secondValue = (int) $data['second'];

        $history = new History();
        $history->setFirstIn($firstValue);
        $history->setSecondIn($secondValue);
        $history->setCreatedAt(new \DateTime());
        $history->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($history);
        $this->entityManager->flush();

        list($firstValue, $secondValue) = [$secondValue, $firstValue];

        $history->setFirstOut($firstValue);
        $history->setSecondOut($secondValue);
        $history->setUpdatedAt(new \DateTime());

        $this->entityManager->flush();

        return $this->json(['result' => 'success']);
    }

     /**
     * @Route("/history", name="history", methods={"GET", "POST"})
     */
    public function getHistory(Request $request): JsonResponse
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);
        $sort = $request->query->get('sort', 'createdAt');
        $order = $request->query->get('order', 'desc');

        $repository = $this->getDoctrine()->getRepository(History::class);
        $queryBuilder = $repository->createQueryBuilder('h')
            ->orderBy('h.' . $sort, $order);

        $pagination = $this->paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            $limit
        );

        return $this->json($pagination->getItems());
    }
}


