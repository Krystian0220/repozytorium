<?php //src/Controller/ApiController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/action", name="api_action", methods={"POST"})
     */
    public function api(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['field1']) || empty($data['field2'])) {
            return $this->json(['error' => 'Missing required fields'], 400);
        }

        return $this->json(['result' => 'success']);
    }
}